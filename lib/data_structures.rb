# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  sorted_arr = arr.sort
  sorted_arr[-1] - sorted_arr[0]
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ["a", "e", "i", "o", "u"]
  letters = str.downcase.chars
  count = 0
  letters.map do |char|
    count += 1 if vowels.include?(char)
  end
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = ["a", "e", "i", "o", "u",
  "A", "E", "I", "O", "U"]
  letters = str.chars
  letters.map do |char|
    letters.delete(char) if vowels.include?(char)
  end
  letters.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  string_arr = int.to_s.chars
  sorted_arr = string_arr.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  letters = str.downcase.chars
  if letters == letters.uniq
    false
  else
    true
  end
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  zip_code = arr[0, 3].join.to_i
  first_three_digits = arr[3, 3].join.to_i
  last_four_digits = arr[6, 4].join.to_i
  "(#{zip_code}) #{first_three_digits}-#{last_four_digits}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  split_str = str.split(",").sort
  split_str[-1].to_i - split_str[0].to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset < 0
    offset = arr.length - offset.abs
  elsif offset > arr.length
    offset = offset % arr.length
  end
  rotated_arr = arr.drop(offset)
  rotated_arr.concat(arr.take(offset))
  rotated_arr
end
